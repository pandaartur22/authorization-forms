//формы почты и пароля
let emailAuth = document.querySelector('#emailAuth');
let passwordAuth = document.querySelector('#passwordAuth');

let submitAuth = document.querySelector('#submitAuth'); //кнопка входа
let emailRegistration = document.querySelector('#emailReg');

let passwordRegistration = document.querySelector('#passwordReg');

//кнопка регистрации
let submitRegistration = document.querySelector('#submitReg');

//список ошибок
let errors = document.querySelectorAll('.error');
console.log(errors);
let formTitle = document.querySelectorAll('.form-span');

// переключение форм
let regButton = document.querySelector('#btnReg');
let authButton = document.querySelector('#btnAuth');
let authorization = document.querySelector('.main-authorization');
let registration = document.querySelector('.main-registration');

regButton.addEventListener('click', () => {
  authorization.classList.add('main-inactive');
  registration.classList.remove('main-inactive');
  emailAuth.value = '';
  passwordAuth.value = '';
});
authButton.addEventListener('click', () => {
  registration.classList.add('main-inactive');
  authorization.classList.remove('main-inactive');
  emailRegistration.value = '';
  passwordRegistration.value = '';
});

// проверка строк в форме

submitAuth.addEventListener('click', (e) => {
  e.preventDefault();

  if (emailAuth.value == '') {
    errors[0].style.zIndex = '0';
    errors[0].textContent = 'Поле обязательно для заполнения';
    emailAuth.style.borderColor = '#CB2424';
    formTitle[0].style.color = '#CB2424';
  } else {
    errors[0].style.zIndex = '-1';
    emailAuth.style.borderColor = '#787878';
    formTitle[0].style.color = '#787878';
  }

  if (passwordAuth.value == '') {
    errors[1].style.zIndex = '0';
    errors[1].textContent = 'Поле обязательно для заполнения';
    passwordAuth.style.borderColor = '#CB2424';
    formTitle[1].style.color = '#CB2424';
  } else {
    errors[1].style.zIndex = '-1';
    passwordAuth.style.borderColor = '#787878';
    formTitle[1].style.color = '#787878';
  }
});

// Регистрация пользователя

submitRegistration.addEventListener('click', (e) => {
  e.preventDefault();
  if (emailRegistration.value == '') {
    errors[2].style.zIndex = '0';
    errors[2].textContent = 'Поле обязательно для заполнения';
    emailRegistration.style.borderColor = '#CB2424';
    formTitle[2].style.color = '#CB2424';
  } else if (
    !emailRegistration.value.includes('@') ||
    emailRegistration.value.length < 8
  ) {
    errors[2].style.zIndex = '0';
    errors[2].textContent = 'Некорректный email';
    emailRegistration.style.borderColor = '#CB2424';
    formTitle[2].style.color = '#CB2424';
  } else {
    errors[2].style.zIndex = '-1';
    emailRegistration.style.borderColor = '#787878';
    formTitle[2].style.color = '#787878';
  }

  if (passwordRegistration.value.trim() == '') {
    errors[3].style.zIndex = '0';
    errors[3].textContent = 'Поле обязательно для заполнения';
    passwordRegistration.style.borderColor = '#CB2424';
    formTitle[3].style.color = '#CB2424';
  } else if (passwordRegistration.value.length < 8) {
    errors[3].style.zIndex = '0';
    errors[3].textContent = 'Пароль должен содержать минимум 8 символов';
    passwordRegistration.style.borderColor = '#CB2424';
    formTitle[3].style.color = '#CB2424';
  } else {
    errors[3].style.zIndex = '-1';
    passwordRegistration.style.borderColor = '#787878';
    formTitle[3].style.color = '#787878';
  }
});

submitRegistration.addEventListener('click', () => {
  if (
    emailRegistration.value !== '' &&
    emailRegistration.value.length > 8 &&
    passwordRegistration.value.length > 7
  ) {
    let users = JSON.parse(localStorage.getItem('users')) || [];
    let isUser = users.some((item) => {
      item.email === emailRegistration.value;
      return;
    });
    console.log(isUser);

    if (isUser) {
      errors[2].style.zIndex = '0';
      errors[2].textContent = 'Пользователь с таким email уже существует';
    } else {
      users.push({
        email: emailRegistration.value,
        password: passwordRegistration.value,
      });
    }
    localStorage.setItem('users', JSON.stringify(users));
  }
});

submitAuth.addEventListener('click', () => {
  if (
    emailAuth.value !== '' &&
    passwordAuth.value !== ''
  ) {
    let users = JSON.parse(localStorage.getItem('users')) || [];
    let user = users.find(
      (user) =>
        user.email === emailAuth.value &&
        user.password === passwordAuth.value,
    );

    if (user) {
      setTimeout(() => {
        window.location.href = 'https://pandart4643.github.io/hangman-game/';
      }, 1000);
    } else {
      errors[0].style.zIndex = '0';
      errors[0].textContent = 'Неправильный email или пароль';
    }
  }
});
